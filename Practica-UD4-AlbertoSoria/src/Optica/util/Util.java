package Optica.util;

import javax.swing.*;

/**
 * Clase Util, invoca las ventanas emergentes de información
 */
public class Util {
    /**
     * Muestra el mensaje
     *
     * @param mensaje
     */
    public static void mensaje(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
