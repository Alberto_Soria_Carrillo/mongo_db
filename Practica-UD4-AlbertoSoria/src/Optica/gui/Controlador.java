package Optica.gui;

import Optica.base.Montura;
import Optica.base.Cliente;
import Optica.base.Ojo;
import Optica.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Clase Controlador, se encarga de la relacion entra las clases y la interacción entre las mismas
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    //Atributos
    private Modelo modelo;
    private Vista vista;

    // Constructor
    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            vista.setTitle("Optica - Conectado");
            setBotonesActivados(true);
            listarclientes();
            listarojos();
            listarmonturas();
        } catch (Exception ex) {
            Util.mensaje("Fallo al conectar");
        }
    }

    /**
     * Implementa los ActionListener
     *
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnAddojo.addActionListener(listener);
        vista.btnModojo.addActionListener(listener);
        vista.btnDelojo.addActionListener(listener);
        vista.btnAddCliente.addActionListener(listener);
        vista.btnModCliente.addActionListener(listener);
        vista.btnDelCliente.addActionListener(listener);
        vista.btnAddmontura.addActionListener(listener);
        vista.btnModmontura.addActionListener(listener);
        vista.btnDelmontura.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    /**
     * Implementa los ListSelectionListener
     *
     * @param listener 
     */
    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.listojo.addListSelectionListener(listener);
        vista.listCliente.addListSelectionListener(listener);
        vista.listmonturas.addListSelectionListener(listener);
    }

    /**
     * Implementa los KeyListener
     *
     * @param listener 
     */
    private void addKeyListeners(KeyListener listener) {
        vista.txtBuscarOjo.addKeyListener(listener);
        vista.txtBuscarCliente.addKeyListener(listener);
        vista.txtBuscarmontura.addKeyListener(listener);
    }

    /**
     * Realiza las acciones generadas por los eventos de los botones
     *
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "conexion":
                try {
                    if (modelo.getCliente() == null) {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        vista.setTitle("Optica - Conectado");
                        setBotonesActivados(true);
                        listarclientes();
                        listarojos();
                        listarmonturas();
                    } else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.setTitle("Optica - Desconectado");
                        setBotonesActivados(false);
                        vista.dlmclientes.clear();
                        vista.dlmojos.clear();
                        vista.dlmmonturas.clear();
                        limpiarCamposojo();
                        limpiarCamposCliente();
                        limpiarCamposmontura();
                    }
                } catch (Exception ex) {
                    Util.mensaje("Fallo de conexión");
                }
                break;

            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "addojo":
                if (comprobarCamposojo()) {
                    modelo.guardarObjeto(new Ojo(vista.txtNombreojo.getText(),
                            Integer.parseInt(vista.txtNumeroPersonas.getText()),
                            Float.parseFloat(vista.txtPrecioojo.getText())));
                    limpiarCamposojo();
                } else {
                    Util.mensaje("Fallo al agregar");
                }
                listarojos();
                break;

            case "modojo":
                if (vista.listojo.getSelectedValue() != null) {
                    if (comprobarCamposojo()) {
                        Ojo ojo = vista.listojo.getSelectedValue();
                        ojo.setNombre(vista.txtNombreojo.getText());
                        ojo.setnumPerson(Integer.parseInt(vista.txtNumeroPersonas.getText()));
                        ojo.setPrecio(Float.parseFloat(vista.txtPrecioojo.getText()));
                        modelo.modificarObjeto(ojo);
                        limpiarCamposojo();
                    } else {
                        Util.mensaje("Fallo al modificar");
                    }
                    listarojos();
                } else {
                    Util.mensaje("No hay ningún elemento seleccionado.");
                }
                break;

            case "delojo":
                if (vista.listojo.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listojo.getSelectedValue());
                    listarojos();
                    limpiarCamposojo();
                } else {
                    Util.mensaje("No hay ningún elemento seleccionado.");
                }
                break;

            case "addCliente":
                if (comprobarCamposCliente()) {
                    modelo.guardarObjeto(new Cliente(vista.txtNombreCliente.getText(),
                            vista.txtApellidosCliente.getText(),
                            vista.dateNacimientoCliente.getDate()));
                    limpiarCamposCliente();
                } else {
                    Util.mensaje("Fallo al agregar");
                }
                listarclientes();
                break;

            case "modCliente":
                if (vista.listCliente.getSelectedValue() != null) {
                    if (comprobarCamposCliente()) {
                        Cliente cliente = vista.listCliente.getSelectedValue();
                        cliente.setNombre(vista.txtNombreCliente.getText());
                        cliente.setApellidos(vista.txtApellidosCliente.getText());
                        cliente.setNacimiento(vista.dateNacimientoCliente.getDate());
                        modelo.modificarObjeto(cliente);
                        limpiarCamposCliente();
                    } else {
                        Util.mensaje("Fallo al modificar");
                    }
                    listarclientes();
                } else {
                    Util.mensaje("No hay ningún elemento seleccionado.");
                }
                break;

            case "delCliente":
                if (vista.listCliente.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listCliente.getSelectedValue());
                    listarclientes();
                    limpiarCamposCliente();
                } else {
                    Util.mensaje("No hay ningún elemento seleccionado.");
                }
                break;

            case "addmontura":
                if (comprobarCamposmontura()) {
                    modelo.guardarObjeto(new Montura(vista.txtmontura.getText()));
                    limpiarCamposmontura();
                } else {
                    Util.mensaje("Fallo al agregar");
                }
                listarmonturas();
                break;

            case "modmontura":
                if (vista.listmonturas.getSelectedValue() != null) {
                    if (comprobarCamposmontura()) {
                        Montura montura = vista.listmonturas.getSelectedValue();
                        montura.setmontura(vista.txtmontura.getText());
                        modelo.modificarObjeto(montura);
                        limpiarCamposmontura();
                    } else {
                        Util.mensaje("Fallo al modificar");
                    }
                    listarmonturas();
                } else {
                    Util.mensaje("No hay ningún elemento seleccionado.");
                }
                break;

            case "delmontura":
                if (vista.listmonturas.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listmonturas.getSelectedValue());
                    listarmonturas();
                    limpiarCamposmontura();
                    break;
                } else {
                    Util.mensaje("No hay ningún elemento seleccionado.");
                }
        }
    }

    /**
     * Revisa lo escrito en las areas de texto de busqueda y muestra el resultado
     *
     * @param e 
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarOjo) {
            listarojosBusqueda(modelo.getOjo(vista.txtBuscarOjo.getText()));
            if (vista.txtBuscarOjo.getText().isEmpty()) {
                vista.dlmojosBusqueda.clear();
            }
        } else if (e.getSource() == vista.txtBuscarCliente) {
            listarclientesBusqueda(modelo.getClientes(vista.txtBuscarCliente.getText()));
            if (vista.txtBuscarCliente.getText().isEmpty()) {
                vista.dlmclientesBusqueda.clear();
            }
        } else if (e.getSource() == vista.txtBuscarmontura) {
            listarmonturasBusqueda(modelo.getmonturas(vista.txtBuscarmontura.getText()));
            if (vista.txtBuscarmontura.getText().isEmpty()) {
                vista.dlmmonturasBusqueda.clear();
            }
        }
    }

    /**
     * Comprueba los cambios realizados en las listas
     *
     * @param e 
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listojo) {
            if (vista.listojo.getSelectedValue() != null) {
                Ojo ojo = vista.listojo.getSelectedValue();
                vista.txtNombreojo.setText(ojo.getNombre());
                vista.txtNumeroPersonas.setText(String.valueOf(ojo.getnumPerson()));
                vista.txtPrecioojo.setText(String.valueOf(ojo.getPrecio()));
            }
        } else if (e.getSource() == vista.listCliente) {
            if (vista.listCliente.getSelectedValue() != null) {
                Cliente cliente = vista.listCliente.getSelectedValue();
                vista.txtNombreCliente.setText(cliente.getNombre());
                vista.txtApellidosCliente.setText(cliente.getApellidos());
                vista.dateNacimientoCliente.setDate(cliente.getNacimiento());
            }
        } else if (e.getSource() == vista.listmonturas) {
            if (vista.listmonturas.getSelectedValue() != null) {
                Montura montura = vista.listmonturas.getSelectedValue();
                vista.txtmontura.setText(montura.getmontura());
            }
        }
    }

    /**
     * Mira si los campos de la clase ojo estan vacios
     *
     * @return
     */
    private boolean comprobarCamposojo() {
        return !vista.txtNombreojo.getText().isEmpty() &&
                !vista.txtNumeroPersonas.getText().isEmpty() &&
                !vista.txtPrecioojo.getText().isEmpty() &&
                comprobarInt(vista.txtNumeroPersonas.getText()) &&
                comprobarFloat(vista.txtPrecioojo.getText());
    }

    /**
     * Mira si los campos de la clase cliente estan vacios
     *
     * @return
     */
    private boolean comprobarCamposCliente() {
        return !vista.txtNombreCliente.getText().isEmpty() &&
                !vista.txtApellidosCliente.getText().isEmpty() &&
                !vista.dateNacimientoCliente.getText().isEmpty();
    }

    /**
     * Mira si los campos de la clase montura estan vacios
     *
     * @return
     */
    private boolean comprobarCamposmontura() {
        return !vista.txtmontura.getText().isEmpty();
    }

    /**
     * Limpia los campos de ojo
     */
    private void limpiarCamposojo() {
        vista.txtNombreojo.setText("");
        vista.txtNumeroPersonas.setText("");
        vista.txtPrecioojo.setText("");
        vista.txtBuscarOjo.setText("");
    }

    /**
     * Limpia los campos de cliente
     */
    private void limpiarCamposCliente() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.dateNacimientoCliente.clear();
        vista.txtBuscarCliente.setText("");
    }

    /**
     * Limpia los campos de montura
     */
    private void limpiarCamposmontura() {
        vista.txtmontura.setText("");
        vista.txtBuscarmontura.setText("");
    }

    /**
     * metodo que comprueba si es un campo int
     *
     * @param txt 
     * @return
     */
    private boolean comprobarInt(String txt) {
        try {
            Integer.parseInt(txt);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * Metodo que comprueba si es un campo float
     *
     * @param txt 
     * @return
     */
    private boolean comprobarFloat(String txt) {
        try {
            Float.parseFloat(txt);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * Metodo que lista los clientes
     */
    private void listarclientes() {
        vista.dlmclientes.clear();
        for (Cliente cliente : modelo.getClientes()) {
            vista.dlmclientes.addElement(cliente);
        }
    }

    /**
     * Metodo que lista los ojos
     */
    private void listarojos() {
        vista.dlmojos.clear();
        for (Ojo ojo : modelo.getOjo()) {
            vista.dlmojos.addElement(ojo);
        }
    }

    /**
     * Metodo que lista los monturas
     */
    private void listarmonturas() {
        vista.dlmmonturas.clear();
        for (Montura montura : modelo.getmonturas()) {
            vista.dlmmonturas.addElement(montura);
        }
    }

    /**
     * Metodo qye lista la clase cliente y busca si hay alguno con ese nombre
     *
     * @param lista
     */
    private void listarclientesBusqueda(ArrayList<Cliente> lista) {
        vista.dlmclientesBusqueda.clear();
        for (Cliente cliente : lista) {
            vista.dlmclientesBusqueda.addElement(cliente);
        }
    }

    /**
     * Metodo qye lista la clase ojo y busca si hay alguno con ese nombre
     *
     * @param lista
     */
    private void listarojosBusqueda(ArrayList<Ojo> lista) {
        vista.dlmojosBusqueda.clear();
        for (Ojo ojo : lista) {
            vista.dlmojosBusqueda.addElement(ojo);
        }
    }

    /**
     * Metodo qye lista la clase montura y busca si hay alguno con ese nombre
     *
     * @param lista
     */
    private void listarmonturasBusqueda(ArrayList<Montura> lista) {
        vista.dlmmonturasBusqueda.clear();
        for (Montura montura : lista) {
            vista.dlmmonturasBusqueda.addElement(montura);
        }
    }

    /**
     * Activa los botones cuando se conecta con mongo
     *
     * @param aBoolean
     */
    private void setBotonesActivados(boolean aBoolean) {
        vista.btnAddojo.setEnabled(aBoolean);
        vista.btnModojo.setEnabled(aBoolean);
        vista.btnDelojo.setEnabled(aBoolean);
        vista.btnAddCliente.setEnabled(aBoolean);
        vista.btnModCliente.setEnabled(aBoolean);
        vista.btnDelCliente.setEnabled(aBoolean);
        vista.btnAddmontura.setEnabled(aBoolean);
        vista.btnModmontura.setEnabled(aBoolean);
        vista.btnDelmontura.setEnabled(aBoolean);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }
}
