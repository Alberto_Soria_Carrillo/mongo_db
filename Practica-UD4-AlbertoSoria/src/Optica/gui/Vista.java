package Optica.gui;

import Optica.base.Montura;
import Optica.base.Ojo;
import Optica.base.Cliente;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista, encarga de los elementos gráficos del programa
 */
public class Vista extends JFrame {

    // Atributos
    private JPanel panelPrincipal;

    // ojos
    JTextField txtNombreojo;
    JTextField txtNumeroPersonas;
    JTextField txtPrecioojo;

    JList<Ojo> listojo;

    JButton btnAddojo;
    JButton btnModojo;
    JButton btnDelojo;

    JTextField txtBuscarOjo;
    JList<Ojo> listBusquedaojo;

    // cliente
    JTextField txtNombreCliente;
    JTextField txtApellidosCliente;
    DatePicker dateNacimientoCliente;

    JButton btnAddCliente;
    JButton btnModCliente;
    JButton btnDelCliente;

    JTextField txtBuscarCliente;
    JList<Cliente> listBusquedaCliente;

    JList<Cliente> listCliente;

    // monturas
    JTextField txtmontura;

    JList<Montura> listmonturas;

    JButton btnAddmontura;
    JButton btnModmontura;
    JButton btnDelmontura;

    JTextField txtBuscarmontura;
    JList<Montura> listBusquedamontura;


    // Modelos
    DefaultListModel<Ojo> dlmojos;
    DefaultListModel<Cliente> dlmclientes;
    DefaultListModel<Montura> dlmmonturas;
    DefaultListModel<Ojo> dlmojosBusqueda;
    DefaultListModel<Cliente> dlmclientesBusqueda;
    DefaultListModel<Montura> dlmmonturasBusqueda;

    // Menu
    JMenuItem itemConectar;
    JMenuItem itemSalir;

    // Constructor
    public Vista() {
        setTitle("Optica - desconectado");
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(800, 650));
        setResizable(false);
        pack();
        setVisible(true);

        startElements();
        startMenu();
    }

    /**
     * Instancia y agrega los elementos
     */
    private void startElements() {
        dlmojos = new DefaultListModel<>();
        listojo.setModel(dlmojos);

        dlmclientes = new DefaultListModel<>();
        listCliente.setModel(dlmclientes);

        dlmmonturas = new DefaultListModel<>();
        listmonturas.setModel(dlmmonturas);

        dlmojosBusqueda = new DefaultListModel<>();
        listBusquedaojo.setModel(dlmojosBusqueda);

        dlmclientesBusqueda = new DefaultListModel<>();
        listBusquedaCliente.setModel(dlmclientesBusqueda);

        dlmmonturasBusqueda = new DefaultListModel<>();
        listBusquedamontura.setModel(dlmmonturasBusqueda);
    }

    /**
     * Instancia y agrega elmenu
     */
    private void startMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        setJMenuBar(menuBar);
    }
}
