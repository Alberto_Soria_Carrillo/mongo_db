package Optica.gui;

import Optica.base.Montura;
import Optica.base.Cliente;
import Optica.base.Ojo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase Modelo, encarga del control de las acciones
 */
public class Modelo {

    // atributos
    private MongoClient clienteM;
    private MongoCollection<Document> clientes;
    private MongoCollection<Document> ojos;
    private MongoCollection<Document> monturas;

    // Getters and Setters
    public MongoClient getCliente() {
        return clienteM;
    }

    /**
     * Conecta con la BBDD
     */
    public void conectar() {
        clienteM = new MongoClient();
        String DATABASE = "Optica";
        MongoDatabase db = clienteM.getDatabase(DATABASE);

        String COLECCION_CLIENTES = "clientes";
        clientes = db.getCollection(COLECCION_CLIENTES);
        String COLECCION_OJOS = "ojos";
        ojos = db.getCollection(COLECCION_OJOS);
        String COLECCION_MONTURAS = "monturas";
        monturas = db.getCollection(COLECCION_MONTURAS);
    }

    /**
     * Desconecta con la BBDD
     */
    public void desconectar() {
        clienteM.close();
        clienteM = null;
    }

    /**
     * Crea una Lista con los Ojos
     *
     * @return
     */
    public ArrayList<Ojo> getOjo() {
        ArrayList<Ojo> lista = new ArrayList<>();

        for (Document document : ojos.find()) {
            lista.add(documentToojo(document));
        }
        return lista;
    }

    /**
     * Compara la lista con la variable y debuelve el resultado
     *
     * @param comparador
     * @return
     */
    public ArrayList<Ojo> getOjo(String comparador) {
        ArrayList<Ojo> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : ojos.find(query)) {
            lista.add(documentToojo(document));
        }

        return lista;
    }

    /**
     * Crea un listado con los clientes
     *
     * @return
     */
    public ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<>();

        for (Document document : clientes.find()) {
            lista.add(documentToCliente(document));
        }
        return lista;
    }

    /**
     * Compara el listado de clientes ocn la variable dada
     *
     * @param comparador
     * @return
     */
    public ArrayList<Cliente> getClientes(String comparador) {
        ArrayList<Cliente> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : clientes.find(query)) {
            lista.add(documentToCliente(document));
        }

        return lista;
    }

    /**
     * Metodo que lista el montura
     *
     * @return
     */
    public ArrayList<Montura> getmonturas() {
        ArrayList<Montura> lista = new ArrayList<>();

        for (Document document : monturas.find()) {
            lista.add(documentTomontura(document));
        }
        return lista;
    }

    /**
     * Compara la variable con la información ne la lista y debuelve el resultado
     *
     * @param comparador
     * @return
     */
    public ArrayList<Montura> getmonturas(String comparador) {
        ArrayList<Montura> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("montura", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : monturas.find(query)) {
            lista.add(documentTomontura(document));
        }

        return lista;
    }

    /**
     * Guarda el objeto en la BBDD
     *
     * @param obj
     */
    public void guardarObjeto(Object obj) {
        if (obj instanceof Ojo) {
            ojos.insertOne(objectToDocument(obj));

        } else if (obj instanceof Cliente) {
            clientes.insertOne(objectToDocument(obj));

        } else if (obj instanceof Montura) {
            monturas.insertOne(objectToDocument(obj));
        }
    }

    /**
     * Modifica el objeto en la BBDD
     *
     * @param obj
     */
    public void modificarObjeto(Object obj) {
        if (obj instanceof Ojo) {
            Ojo ojo = (Ojo) obj;
            ojos.replaceOne(new Document("_id", ojo.getId()), objectToDocument(ojo));

        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;
            clientes.replaceOne(new Document("_id", cliente.getId()), objectToDocument(cliente));

        } else if (obj instanceof Montura) {
            Montura montura = (Montura) obj;
            monturas.replaceOne(new Document("_id", montura.getId()), objectToDocument(montura));
        }
    }

    /**
     * elimina el objeto de la BBDD
     *
     * @param obj
     */
    public void eliminarObjeto(Object obj) {
        if (obj instanceof Ojo) {
            Ojo ojo = (Ojo) obj;
            ojos.deleteOne(objectToDocument(ojo));

        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;
            clientes.deleteOne(objectToDocument(cliente));

        } else if (obj instanceof Montura) {
            Montura montura = (Montura) obj;
            monturas.deleteOne(objectToDocument(montura));
        }
    }

    /**
     * crea un nuevo documento ojo en la BBDD
     *
     * @param dc
     * @return
     */
    public Ojo documentToojo(Document dc) {
        Ojo ojo = new Ojo();

        ojo.setId(dc.getObjectId("_id"));
        ojo.setNombre(dc.getString("nombre"));
        ojo.setnumPerson(dc.getInteger("numPerson"));
        ojo.setPrecio((Float.parseFloat(String.valueOf(dc.getDouble("precio")))));
        return ojo;
    }

    /**
     * crea un nuevo cliente ne la BBDD
     *
     * @param dc
     * @return
     */
    public Cliente documentToCliente(Document dc) {
        Cliente cliente = new Cliente();

        cliente.setId(dc.getObjectId("_id"));
        cliente.setNombre(dc.getString("nombre"));
        cliente.setApellidos(dc.getString("apellidos"));
        cliente.setNacimiento(LocalDate.parse(dc.getString("nacimiento")));
        return cliente;
    }

    /**
     * crea una nueva montura en la BBDD
     *
     * @param dc
     * @return
     */
    public Montura documentTomontura(Document dc) {
        Montura montura = new Montura();

        montura.setId(dc.getObjectId("_id"));
        montura.setmontura(dc.getString("montura"));
        return montura;
    }

    /**
     * Crea los objetos de todas las clases
     *
     * @param obj
     * @return
     */
    public Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Ojo) {
            Ojo ojo = (Ojo) obj;

            dc.append("nombre", ojo.getNombre());
            dc.append("numPerson", ojo.getnumPerson());
            dc.append("precio", ojo.getPrecio());
        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;

            dc.append("nombre", cliente.getNombre());
            dc.append("apellidos", cliente.getApellidos());
            dc.append("nacimiento", cliente.getNacimiento().toString());

        } else if (obj instanceof Montura) {
            Montura montura = (Montura) obj;

            dc.append("montura", montura.getmontura());
        } else {
            return null;
        }
        return dc;
    }
}
