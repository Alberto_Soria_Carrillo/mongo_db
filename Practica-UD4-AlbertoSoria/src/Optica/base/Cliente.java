package Optica.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * Clase Cliente, guarda la estructura de los datos de los Clientes en java
 */
public class Cliente {

    // Atributos
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private LocalDate nacimiento;

    // Constructor
    public Cliente(String nombre, String apellidos, LocalDate nacimiento) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacimiento = nacimiento;
    }

    public Cliente() {
    }

    // Getters and Setters
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LocalDate getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(LocalDate nacimiento) {
        this.nacimiento = nacimiento;
    }

    @Override
    public String toString() {
        return nombre + " " + apellidos;
    }
}
