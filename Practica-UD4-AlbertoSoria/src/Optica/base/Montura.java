package Optica.base;

import org.bson.types.ObjectId;

/**
 * Clase Montura, guarda la estructura de los datos de las monturas en java
 */
public class Montura {

    //Atributos
    private ObjectId id;
    private String montura;

    // Constructor
    public Montura(String montura) {
        this.montura = montura;
    }

    public Montura() {

    }

    // Getters and Setter
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getmontura() {
        return montura;
    }

    public void setmontura(String montura) {
        this.montura = montura;
    }

    @Override
    public String toString() {
        return montura;
    }
}
