package Optica.base;

import org.bson.types.ObjectId;

/**
 *Clase Ojo, guarda la estructura de los datos de los ojos en java
 */
public class Ojo {

    //Atributos
    private ObjectId id;
    private String nombre;
    private int numPerson;
    private float precio;

    // Constructor
    public Ojo(String nombre, int numPerson, float precio) {
        this.nombre = nombre;
        this.numPerson = numPerson;
        this.precio = precio;
    }

    public Ojo() {

    }

    //Getter y setters, para poder se implementados desde otra clase
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getnumPerson() {
        return numPerson;
    }

    public void setnumPerson(int numPerson) {
        this.numPerson = numPerson;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return nombre + " - " + numPerson + "- " + precio + " €";
    }
}
