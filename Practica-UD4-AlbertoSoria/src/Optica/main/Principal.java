package Optica.main;

import Optica.gui.Controlador;
import Optica.gui.Modelo;
import Optica.gui.Vista;

/**
 * Clase donde arranca el programa
 */
public class Principal {
    /**
     * Metodo Principal, inicia el programa
     * @param args
     */
    public static void main(String[] args) {
        new Controlador(new Modelo(), new Vista());
    }
}
